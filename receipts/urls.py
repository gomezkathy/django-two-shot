from django.urls import path
from receipts.views import create_account, create_category, account_list, receipt_list_view, create_receipt, category_list

urlpatterns = [
    path("", receipt_list_view, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", category_list, name="category_list"),
    path("accounts/", account_list, name="account_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account")
]

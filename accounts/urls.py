from django.urls import path
from accounts.views import login_user, log_out, signup_user

urlpatterns = [
   path("login/",login_user, name="login"),
   path("logout/", log_out, name="logout"),
   path("signup/", signup_user, name="signup" ),
]
